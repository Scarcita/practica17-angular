import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  constructor() { }

  mensajePadre! : string;
  SecondMessage! : string;
  thirdMessage!: string;
  fourthMessage!: string;
  fifthMessage!: string;


  recibirMensaje ($event: string): void {
    this.mensajePadre = $event;
    console.log(this.mensajePadre);  
  }

  recibirSegundo ($event: string): void {
    this.SecondMessage = $event;
  }

  recibirTercer ($event: string): void {
    this.thirdMessage = $event;
  }

  recibirCuarto($event: string): void {
    this.fourthMessage = $event;
  }

  recibirQuinto ($event: string): void {
    this.fifthMessage = $event;
  }

  ngOnInit(): void {
  }

}
